{
    "$schema": "http://json-schema.org/draft-07/schema#",
    "title": "JSON Schema for WorldSynth Minecraft materials",
    "type": "object",
    "additionalProperties": false,
    "properties": {
        "minecraft": {
            "type": "object",
            "description": "Minecraft profile",
            "patternProperties": {
                "^[\\w:]+$": {
                    "type": "object",
                    "description": "Material declaration",
                    "properties": {
                        "displayName": {
                            "type": "string",
                            "description": "Display name for the material shown in WorldSynth's UI"
                        },
                        "color": {
                            "type": "array",
                            "description": "RGB(A) values used to represent the material in WorldSynth's UI for preview maps",
                            "minItems": 3,
                            "maxItems": 4,
                            "items": {
                                "type": "number"
                            }
                        },
                        "texture": {
                            "type": "string",
                            "description": "Relative path (from material file) to the texture image file for this material"
                        },
                        "tags": {
                            "type": "array",
                            "description": "Array of filterable tags for the material",
                            "items": {
                                "type": "string"
                            }
                        },
                        "isAir": {
                            "type": "boolean",
                            "description": "true if material is air material"
                        },
                        "lightLevel": {
                            "type": "integer",
                            "description": "Light level for light emiting blocks. Range 0 - 15. Default: 0.",
                            "minimum": 0,
                            "maximum": 15
                            
                        },
                        "lightDampening": {
                            "type": "integer",
                            "description": "Light attenuation of a blocks. Range 0 - 15. 0 is transparent, 1 - 14 is light filtering, 15 is opaque (solid). Default: 15.",
                            "minimum": 0,
                            "maximum": 15
                        },
                        "tileEntity": {
                            "type": "string",
                            "description": "Relative path (from material file) to the nbt file of the tile entity for this material"
                        },
                        "tileTick": {
                            "type": "string",
                            "description": "Relative path (from material file) to the nbt file of the tile tick for this material"
                        },
                        "liquidTick": {
                            "type": "string",
                            "description": "Relative path (from material file) to the nbt file of the liquid tick for this material"
                        },
                        "anvil_1.12": {"$ref": "#/definitions/anvil_data_1.12.2_down"},
                        "anvil_1.13": {"$ref": "#/definitions/anvil_data_1.13_up"},
                        "anvil_1.14": {"$ref": "#/definitions/anvil_data_1.13_up"},
                        "anvil_1.15": {"$ref": "#/definitions/anvil_data_1.13_up"},
                        "anvil_1.16": {"$ref": "#/definitions/anvil_data_1.13_up"},
                        "anvil_1.17": {"$ref": "#/definitions/anvil_data_1.13_up"},
                        "anvil_1.18": {"$ref": "#/definitions/anvil_data_1.13_up"},
                        "anvil_1.19": {"$ref": "#/definitions/anvil_data_1.13_up"},
                        "anvil_1.20": {"$ref": "#/definitions/anvil_data_1.13_up"},
                        "properties": {
                            "type": "object",
                            "description": "Properties that define the states of the material",
                            "patternProperties": {
                                "^\\w+$": {
                                    "type": "array",
                                    "description": "Property",
                                    "items": {
                                        "type": "string"
                                    }
                                }
                            },
                            "additionalProperties": false
                        },
                        "states": {
                            "type": "array",
                            "description": "States of the material",
                            "items": {"$ref": "#/definitions/state"}
                        }
                    },
                    "patternProperties": {
                        "^anvil_1(\\.\\d+)+$": {"$ref": "#/definitions/anvil_data_1.13_up"}
                    },
                    "additionalProperties": false
                }
            },
            "additionalProperties": false
        }
    },
    "definitions": {
        "anvil_data_1.12.2_down": {
            "type": "object",
            "description": "Anvil data for 1.12.2 and older versions",
            "properties": {
                "id": {
                    "type": "number",
                    "description": "Numeric block ID"
                },
                "meta": {
                    "type": "number",
                    "description": "Numeric block meta data"
                },
                "tileEntity": {
                    "type": "string",
                    "description": "Relative path (from material file) to the nbt file of the tile entity"
                },
                "tileTick": {
                    "type": "string",
                    "description": "Relative path (from material file) to the nbt file of the tile tick"
                },
                "liquidTick": {
                    "type": "string",
                    "description": "Relative path (from material file) to the nbt file of the liquid tick"
                }
            },
            "additionalProperties": false
        },
        "anvil_data_1.13_up": {
            "type": "object",
            "description": "Anvil data for 1.13 and newer versions",
            "properties": {
                "id": {
                    "type": "string",
                    "description": "Flattened block id"
                },
                "properties": {
                    "type": "object",
                    "description": "State values of the material",
                    "properties": {
                        "items": {
                            "type": "string"
                        }
                    }
                },
                "tileEntity": {
                    "type": "string",
                    "description": "Relative path (from material file) to the nbt file of the tile entity"
                },
                "tileTick": {
                    "type": "string",
                    "description": "Relative path (from material file) to the nbt file of the tile tick"
                },
                "liquidTick": {
                    "type": "string",
                    "description": "Relative path (from material file) to the nbt file of the liquid tick"
                }
            },
            "additionalProperties": false
        },
        "state": {
            "type": "object",
            "description": "A state of the material",
            "properties": {
                "properties": {
                    "type": "object",
                    "description": "Propery values of the stat",
                    "patternProperties": {
                        "^\\w+$": {
                            "type": "string",
                            "description": "Property value"
                        }
                    },
                    "additionalProperties": false
                },
                "default": {
                    "type": "boolean",
                    "description": "true if this state is the default state of the material"
                },
                "displayName": {
                    "type": "string",
                    "description": "Display name for the material state shown in WorldSynth's UI"
                },
                "color": {
                    "type": "array",
                    "description": "RGB(A) values used to represent the material in WorldSynth's UI for preview maps",
                    "minItems": 3,
                    "maxItems": 4,
                    "items": {
                        "type": "number"
                    }
                },
                "texture": {
                    "type": "string",
                    "description": "Relative path (from material file) to the texture image file for this material state"
                },
                "lightLevel": {
                    "type": "integer",
                    "description": "Light level for light emiting blocks. Range 0 - 15.",
                    "minimum": 0,
                    "maximum": 15
                    
                },
                "lightDampening": {
                    "type": "integer",
                    "description": "Light attenuation of a blocks. Range 0 - 15. 0 is transparent, 1 - 14 is light filtering, 15 is opaque (solid).",
                    "minimum": 0,
                    "maximum": 15
                },
                "tileEntity": {
                    "type": "string",
                    "description": "Relative path (from material file) to the nbt file of the tile entity for this material state"
                },
                "tileTick": {
                    "type": "string",
                    "description": "Relative path (from material file) to the nbt file of the tile tick for this material state"
                },
                "liquidTick": {
                    "type": "string",
                    "description": "Relative path (from material file) to the nbt file of the liquid tick for this material state"
                },
                "anvil_1.12": {"$ref": "#/definitions/anvil_data_1.12.2_down"},
                "anvil_1.13": {"$ref": "#/definitions/anvil_data_1.13_up"},
                "anvil_1.14": {"$ref": "#/definitions/anvil_data_1.13_up"},
                "anvil_1.15": {"$ref": "#/definitions/anvil_data_1.13_up"},
                "anvil_1.16": {"$ref": "#/definitions/anvil_data_1.13_up"},
                "anvil_1.17": {"$ref": "#/definitions/anvil_data_1.13_up"},
                "anvil_1.18": {"$ref": "#/definitions/anvil_data_1.13_up"},
                "anvil_1.19": {"$ref": "#/definitions/anvil_data_1.13_up"},
                "anvil_1.20": {"$ref": "#/definitions/anvil_data_1.13_up"}
            },
            "patternProperties": {
                "^anvil_1(\\.\\d+)+$": {
                    "oneOf": [
                        {"$ref": "#/definitions/anvil_data_1.12.2_down"},
                        {"$ref": "#/definitions/anvil_data_1.13_up"}
                    ]
                }
            },
            "additionalProperties": false
        }
    }
}