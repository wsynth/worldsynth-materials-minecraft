import json

# Merges new and old material files such that old definitions are kept where the same material exists in the new file.

# New materials file
new_materials_path = "processing/json/blocks.json"
# Old materials file
old_materials_path = "materials/blocks.json"

# Output materials file
output_materials_path = "processing/json/blocks_merged.json"

# Materials to exclude in merged results
excluded_materials = [
    "minecraft:air",
    "minecraft:bedrock"
]

# Materials to exclude merging from old
excluded_old_materials = [
    # List materials to not merge in from old
]



with open(new_materials_path, "r") as new_materials_file:
    new_materials = json.load(new_materials_file)["minecraft"]

with open(old_materials_path, "r") as old_materials_file:
    old_materials = json.load(old_materials_file)["minecraft"]

mergedMaterials = {}
for m_key in new_materials:
    if m_key in excluded_materials:
        continue

    if m_key in old_materials and m_key not in excluded_old_materials:
        mergedMaterials[m_key] = old_materials[m_key]
        print(m_key)
    else:
        mergedMaterials[m_key] = new_materials[m_key]
        print("\t\t\t" + m_key)

mergedMaterials = { "minecraft": mergedMaterials }
with open(output_materials_path, "w") as merged_materials_file:
    json.dump(mergedMaterials, merged_materials_file, indent=4)