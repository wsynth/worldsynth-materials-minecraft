import json
import os

# Input materials file path
materials_filepath = "processing/json/blocks.json"

# Input textures path
textures_dirpath = "materials/textures/block"

# Output materials file path
output_materials_path = "processing/json/blocks_textured.json"



with open(materials_filepath, "r") as materials_file:
    materials = json.load(materials_file)["minecraft"]

textures_filenames = os.listdir(textures_dirpath)

for m_key in materials:
    sugestedMaterialTexture_filename = m_key.replace("minecraft:", "") + ".png"
    sugestedMaterialTexture_filepath = "textures/block/" + sugestedMaterialTexture_filename
    # print(sugestedMaterialTexture_filepath)
    if sugestedMaterialTexture_filename in textures_filenames:
        materials[m_key]["texture"] = sugestedMaterialTexture_filepath
        print(m_key + ":\n\t" + sugestedMaterialTexture_filename)

materials = { "minecraft": materials }
with open(output_materials_path, "w") as materials_file:
    json.dump(materials, materials_file, indent=4)